//
//  TextViewController.swift
//  hot
//
//  Created by Chi Chun Hung on 2017/7/23.
//  Copyright © 2017年 ChiChun Hung. All rights reserved.
//

import UIKit

class TextViewController: UIViewController, UIWebViewDelegate {
    var urlString: String!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(self.urlString)
        let url = URL(string: self.urlString)
        let request = URLRequest(url: url!)
        self.webView.loadRequest(request)
        self.webView.delegate = self
        // Do any additional setup after loading the view.
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if self.webView.canGoBack{
            self.goBackBtn.isEnabled = true
        }else{
            self.goBackBtn.isEnabled = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func refresh(_ sender: Any) {
    }
    @IBOutlet weak var goBackBtn: UIBarButtonItem!
    @IBAction func goBack(_ sender: Any) {
        self.webView.goBack()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
